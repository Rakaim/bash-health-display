#!/bin/bash     

###################
### COLORS DEFI ###
###################
red='\033[0;31m'
yellow='\033[1;33m'
white='\033[1;37m'
green='\033[0;32m'
NC='\033[0m' # No Color

repeat=1

while [ $repeat == 1 ]
do
	# CPU SECTION
	echo -e $white CPU:$green `top -b -n1 | grep "Cpu(s)" | awk '{print $2 + $4}'` 
	FREE_DATA=`free -m | grep Mem` 
	CURRENT=`echo $FREE_DATA | cut -f3 -d' '`
	TOTAL=`echo $FREE_DATA | cut -f2 -d' '`

	# RAM SECTION
	echo -e $white RAM:$green $(echo "scale = 2; $CURRENT/$TOTAL*100" | bc)

	# Storage SECTION
	echo -e $white Storage:$green `df -lh | awk '{if ($6 == "/") { print $5 }}' | head -1 | cut -d'%' -f1`

	echo -e $NC
	sleep 15
done


# bash-health-display

### Purpose

This is a super boring version of topaz or nmon for a long term monitor display. The advantage is that I can expand this to display stats about other servers in my setup, application health, and user counts. *Assumes user is limited to the command line for outputs*.

### Summary

This is a simple looped shell script that parses CLI commands

### Credit

Adapted from: https://unix.stackexchange.com/questions/69167/bash-script-that-print-cpu-usage-diskusage-ram-usage
